<?php

/**
 * @file
 * Contains contacts_team_application.page.inc.
 *
 * Page callback for Team application entities.
 */

use Drupal\contacts_references\Entity\ReferenceType;
use Drupal\Core\Render\Element;

/**
 * Prepares variables for Reference Submissions templates.
 *
 * Default template: contacts_reference.html.twig.
 *
 * Rather than rendering all the fields on the entity, this will prepare
 * variables containing information about the reference.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_contacts_reference(array &$variables) {
  /** @var \Drupal\contacts_references\Entity\Reference $reference */
  $reference = $variables['elements']['#contacts_reference'];

  $variables['content']['heading'] = [
    '#type' => 'html_tag',
    '#tag' => 'h4',
    '#value' => ReferenceType::load($reference->bundle())->label(),
    '#weight' => 0,
  ];

  // Add a surrogate status field, still based on the state field
  // but not rendered using the buttons.
  $variables['content']['status'] = $reference->get('state')->view(['label' => 'inline']);

  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  $variables['content']['separator'] = ['#markup' => '<hr />', '#weight' => 100];
}
