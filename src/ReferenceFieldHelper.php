<?php

namespace Drupal\contacts_references;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Finds reference fields on a content entity.
 *
 * @package Drupal\contacts_references
 */
class ReferenceFieldHelper {

  /**
   * Finds References attached to an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to search.
   *
   * @return \Drupal\contacts_references\Entity\Reference[]
   *   Array of References.
   */
  public function findReferences(ContentEntityInterface $entity) {
    $references = [];

    foreach ($entity->getFields(FALSE) as $field_id => $field) {
      /** @var \Drupal\Core\Field\FieldItemListInterface $field */
      $definition = $field->getFieldDefinition();
      if ($definition->getType() == 'entity_reference' && $definition->getSetting('target_type') === 'contacts_reference') {
        /** @var \Drupal\contacts_references\Entity\ReferenceInterface $reference */
        $references[$field_id] = $field->entity;
      }
    }
    return $references;
  }

}
