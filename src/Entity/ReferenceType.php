<?php

namespace Drupal\contacts_references\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the contacts reference type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "contacts_reference_type",
 *   label = @Translation("Reference type"),
 *   label_collection = @Translation("Reference types"),
 *   label_singular = @Translation("Reference type"),
 *   label_plural = @Translation("Reference types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Reference type",
 *     plural = "@count Reference types"
 *   ),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\contacts_references\Form\ReferenceTypeForm",
 *       "edit" = "Drupal\contacts_references\Form\ReferenceTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "list_builder" = "Drupal\contacts_references\ReferenceTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer contacts reference types",
 *   config_prefix = "type",
 *   bundle_of = "contacts_reference",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "subject",
 *     "body",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/contacts-reference-type/add",
 *     "edit-form" = "/admin/structure/contacts-reference-type/manage/{contacts_reference_type}",
 *     "delete-form" = "/admin/structure/contacts-reference-type/manage/{contacts_reference_type}/delete",
 *     "collection" = "/admin/structure/contacts-reference-type",
 *   },
 * )
 */
class ReferenceType extends ConfigEntityBundleBase implements ConfigEntityInterface {

  /**
   * The machine name of this contacts reference type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the contacts reference type.
   *
   * @var string
   */
  protected $label;

  /**
   * Reference email subject.
   *
   * @var string
   */
  protected $subject;

  /**
   * Reference email body.
   *
   * @var string
   */
  protected $body;

}
