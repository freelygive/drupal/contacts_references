<?php

namespace Drupal\contacts_references\Entity;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;

/**
 * Defines the contacts reference entity class.
 *
 * @ContentEntityType(
 *   id = "contacts_reference",
 *   label = @Translation("Reference"),
 *   label_singular = @Translation("Reference"),
 *   label_plural = @Translation("References"),
 *   label_count = @PluralTranslation(
 *     singular = "@count reference",
 *     plural = "@count references"
 *   ),
 *   bundle_label = @Translation("Type"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\contacts_references\ReferenceListBuilder",
 *     "access" = "Drupal\contacts_references\Entity\ReferenceAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\contacts_references\Form\ReferenceForm",
 *       "edit" = "Drupal\contacts_references\Form\ReferenceForm",
 *       "submit" = "Drupal\contacts_references\Form\ReferenceSubmitForm",
 *       "replace" = "Drupal\contacts_references\Form\ReferenceReplaceForm",
 *     },
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_references\ReferenceHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "contacts_reference",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "bundle",
 *     "uuid" = "uuid",
 *   },
 *   bundle_entity_type = "contacts_reference_type",
 *   admin_permission = "administer contacts references",
 *   field_ui_base_route = "entity.contacts_reference_type.edit_form",
 *   links = {
 *     "edit-form" = "/references/{contacts_reference}/edit",
 *     "submission-form" = "/references/{token}/{uuid}/submit",
 *     "archive-form" = "/references/{contacts_reference}/archive",
 *     "replace-form" = "/references/replace/{c_events_team_app}/{contacts_reference}",
 *     "collection" = "/admin/content/contacts-reference",
 *   }
 * )
 */
class Reference extends ContentEntityBase implements ReferenceInterface {

  use StringTranslationTrait;
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function label() {
    $owner = $this->getOwner();
    $owner_label = $owner ?
      $owner->label() :
      $this->t('Deleted [@uid]', ['@uid' => $this->getOwnerId()]);
    return $this->t('Reference for @owner by @referee', [
      '@owner' => $owner_label,
      '@referee' => $this->getRefereeName(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    return $this->set('created', $timestamp);
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    return $this->set('uid', $account->id());
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    return $this->set('uid', $uid);
  }

  /**
   * {@inheritdoc}
   */
  public function getRefereeName() {
    return $this->get('name')->view()[0]['#markup'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function archive() {
    $this->set('status', 'archived');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmittedTime() : ?int {
    /** @var \Drupal\contacts\StatusLogItemList $log */
    $log = $this->get('status_log');
    return $log->getTimestamp('submitted');
  }

  /**
   * {@inheritdoc}
   */
  public function getApprovedTime() : ?int {
    /** @var \Drupal\contacts\StatusLogItemList $log */
    $log = $this->get('status_log');
    return $log->getTimestamp('approved');
  }

  /**
   * {@inheritdoc}
   */
  public function getRejectedTime() : ?int {
    /** @var \Drupal\contacts\StatusLogItemList $log */
    $log = $this->get('status_log');
    return $log->getTimestamp('rejected');
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Reference for'))
      ->setDescription(t('The user ID of the applicant'))
      ->setSetting('target_type', 'user')
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('name')
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setSetting('components', [
        'title' => TRUE,
        'given' => TRUE,
        'middle' => FALSE,
        'family' => TRUE,
        'generational' => FALSE,
        'credentials' => FALSE,
      ])
      ->setSetting('labels', [
        'title' => new TranslatableMarkup('Title'),
        'given' => new TranslatableMarkup('First name'),
        'family' => new TranslatableMarkup('Last name'),
      ])
      ->setSetting('show_component_required_marker', TRUE)
      ->setSetting('title_display', [
        'title' => 'title',
        'given' => 'title',
        'family' => 'title',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('Status'))
      ->setDescription(t('The reference status.'))
      ->setRequired(TRUE)
      ->setSetting('workflow', 'contacts_references_submission_process')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'state_transition_form',
        'weight' => 100,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status_log'] = BaseFieldDefinition::create('status_log')
      ->setLabel(t('Status History'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('A log of when the status was changed and by whom.'))
      ->setSettings([
        'source_field' => 'state',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time the contacts reference item was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time the contacts reference item was last edited.'));

    $fields['application'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Application'))
      ->setDescription(t('The Team Application for this reference'))
      ->setSetting('target_type', 'c_events_team_app')
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['submitted'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Submitted'))
      ->setDescription(t('The last submitted time for the reference'));

    $fields['sent'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Sent'))
      ->setDescription(t('The last request sent time for the reference'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getTeamApplication() {
    return $this->get('application')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function markAsSubmitted() {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state */
    $state = $this->get('state')->first();
    $workflow = $state->getWorkflow();
    $transitions = $workflow->getAllowedTransitions($state->value, $this);
    if (isset($transitions['submit'])) {
      $state->applyTransition($transitions['submit']);
      $this->set('submitted', \Drupal::time()->getRequestTime());
      $this->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getManuallyAllowedTransitions(AccountInterface $account) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state */
    $state = $this->get('state')->first();

    // If the reference hasn't been submitted yet, then staff should not be able
    // to perform any operations on it.
    if ($state->value == 'pending' || $state->value == 'requested') {
      return [];
    }

    $allowed_manual_transitions = [
      'approve',
      'reject',
      'reset',
    ];

    return $allowed_manual_transitions;
  }

  /**
   * Generates a token used in the public reference submission URL.
   *
   * @return string
   *   The generated token.
   */
  public function generateToken() {
    $private_key = \Drupal::service('private_key');
    $value = $this->uuid() . ':' . $this->get('email')->value;
    return Crypt::hmacBase64($value, $private_key->get() . Settings::getHashSalt());
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $params = parent::urlRouteParameters($rel);

    if ($rel == 'submission-form') {
      // Add token and parameters needed for submission form.
      $token = $this->generateToken();
      $params['token'] = $token;
      $params['uuid'] = $this->uuid();
      // Remove the ID as we're using the uuid.
      unset($params['contacts_reference']);
    }
    elseif ($rel == 'replace-form') {
      $params['c_events_team_app'] = $this->get('application')->target_id;
    }

    return $params;
  }

}
