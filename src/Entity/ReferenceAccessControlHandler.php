<?php

namespace Drupal\contacts_references\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Reference access control handler.
 *
 * @package Drupal\contacts_references\Entity
 */
class ReferenceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * Reference email sender.
   *
   * @var \Drupal\contacts_references\ReferenceSender
   */
  protected $referenceSender;

  /**
   * Constructs a reference access control handler instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   */
  public function __construct(EntityTypeInterface $entity_type) {
    parent::__construct($entity_type);
    $this->referenceSender = \Drupal::service('contacts_references.sender');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\contacts_references\Entity\Reference $entity */

    // Note: 'submit' is not checked here. This is a route-level check performed
    // in ReferenceController::checkSubmitAccess.
    $app = $entity->getTeamApplication();

    switch ($operation) {
      case 'view':
        // If user can view the associated team app
        // then they can also view the reference (provided the application has
        // been submitted).
        return AccessResult::allowedIf($app && $app->access('view', $account) && $app->isSubmitted())
          ->orIf(parent::checkAccess($entity, $operation, $account));

      case 'update':
      case 'resend':
      case 'replace':
        // Reference can only be replaced or updated if:
        // - there's an associated team application.
        // - and the current user can manage the application
        // - and the application has been submitted
        // - and the app has not been accepted/approved/rejected/archived.
        // - and the reference has not been approved/archived.
        $can_replace = $app != NULL;
        $can_replace = $can_replace && $app->access('update', $account);
        $can_replace = $can_replace && $app->isSubmitted();
        $can_replace = $can_replace && in_array($app->get('state')->value, [
          'submitted',
          'references_received',
          'references_reviewed',
        ]);
        $can_replace = $can_replace && in_array($entity->get('state')->value, [
          'pending',
          'requested',
          'submitted',
          'rejected',
        ]);

        // Exception: User can update their own reference if:
        // - the application belongs to the user.
        // - the application has not been submitted.
        // - the reference has not been sent.
        if ($app && $own_update = $app->getOwnerId() == $account->id()) {
          $own_update = $own_update && $operation == 'update';
          $own_update = $own_update && $app->get('state')->value == 'draft';
          $own_update = $own_update && $entity->get('state')->value == 'pending';

          if ($own_update) {
            $can_replace = $own_update;
          }
        }

        // Make sure a reference can be sent.
        if ($operation == 'resend') {
          $can_replace = $can_replace && $this->referenceSender->canSendReference($entity);
        }

        // Nobody can replace the reference if the application is for a team
        // that no longer exists.
        if ($app && $operation == 'replace') {
          $can_replace = $can_replace && $app->getTeam();
        }

        return AccessResult::allowedIf($can_replace)
          ->setCacheMaxAge(0);
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowed();
  }

}
