<?php

namespace Drupal\contacts_references\Entity;

use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an entity for contacts reference items.
 */
interface ReferenceInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the referee's name.
   *
   * @return string
   *   The referee's name.
   */
  public function getRefereeName();

  /**
   * Archive this contacts reference entity.
   *
   * @return $this
   */
  public function archive();

  /**
   * Returns the contacts reference item creation timestamp.
   *
   * @todo Remove and use the new interface when #2833378 is done.
   * @see https://www.drupal.org/node/2833378
   *
   * @return int
   *   Creation timestamp of the contacts reference item.
   */
  public function getCreatedTime();

  /**
   * Sets the contacts reference item creation timestamp.
   *
   * @todo Remove and use the new interface when #2833378 is done.
   * @see https://www.drupal.org/node/2833378
   *
   * @param int $timestamp
   *   The contacts reference creation timestamp.
   *
   * @return \Drupal\contacts_references\Entity\ReferenceInterface
   *   The called contacts reference item.
   */
  public function setCreatedTime($timestamp);

  /**
   * Marks the reference as submitted.
   */
  public function markAsSubmitted();

  /**
   * Gets the team application for this reference request.
   *
   * @return \Drupal\contacts_events_teams\Entity\TeamApplication
   *   The application.
   */
  public function getTeamApplication();

  /**
   * Gets the time the reference was submitted.
   *
   * @return int|null
   *   Timestamp or null.
   */
  public function getSubmittedTime(): ?int;

  /**
   * Gets the time the reference was approved.
   *
   * @return int|null
   *   Timestamp or null.
   */
  public function getApprovedTime(): ?int;

  /**
   * Gets the time the reference was rejected.
   *
   * @return int|null
   *   Timestamp or null.
   */
  public function getRejectedTime(): ?int;

  /**
   * Retreives a collection of transition IDs that the user can apply manually.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return array
   *   Array of transition IDs that the user can invoke.
   */
  public function getManuallyAllowedTransitions(AccountInterface $account);

}
