<?php

namespace Drupal\contacts_references\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Reference submit form.
 */
class ReferenceSubmitForm extends ReferenceFormBase {

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    // Get the reference from the UUID.
    if ($uuid = $route_match->getParameter('uuid')) {
      $results = $this->entityTypeManager->getStorage('contacts_reference')
        ->loadByProperties(['uuid' => $uuid]);

      return reset($results);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // If already submitted just show a message.
    if ($this->entity->getSubmittedTime()) {
      $build = [];
      $build['header'] = ['#markup' => '<h2>Reference Submitted</h2>'];
      $build['message'] = ['#markup' => '<p>Thank you for completing the reference - it has been successfully submitted.</p>'];
      return $build;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Rebuild so we get the submitted message.
    $form_state->setRebuild();
  }

}
