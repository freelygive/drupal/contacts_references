<?php

namespace Drupal\contacts_references\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Reference edit form.
 *
 * @package Drupal\contacts_references\Form
 */
class ReferenceForm extends ReferenceFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $application = $this->entity->getTeamApplication();

    // Extra cancel button to go back to the staff team app page.
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#url' => $application->toUrl('canonical', ['fragment' => 'references']),
      '#title' => $this->t('Cancel'),
      '#attributes' => ['class' => ['btn', 'btn-secondary']],
      '#access' => $application->access('manage'),
      '#weight' => 99,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $application = $this->entity->getTeamApplication();
    $form_state->setRedirectUrl($application->toUrl('canonical', ['fragment' => 'references']));

    return $result;
  }

}
