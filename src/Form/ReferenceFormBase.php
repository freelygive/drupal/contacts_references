<?php

namespace Drupal\contacts_references\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Base class for reference edit forms.
 *
 * Used by the staff edit form, and the referee submission form.
 *
 * @see \Drupal\contacts_references\Form\ReferenceSubmitForm
 * @see \Drupal\contacts_references\Form\ReferenceForm
 */
abstract class ReferenceFormBase extends ContentEntityForm implements TrustedCallbackInterface {

  /**
   * The reference entity.
   *
   * @var \Drupal\contacts_references\Entity\Reference
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function setOperation($operation) {
    // Always use the "submission" form display.
    return parent::setOperation('submit');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $application = $this->entity->getTeamApplication();
    $team = $application->getTeam();
    $ticket = $application->getTicket();
    $event = $team ? $team->getEvent() : $application->get('event')->entity;

    $form['#pre_render'][] = [$this, 'layout'];

    $form['applicant_title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $this->t('Reference For'),
      '#weight' => 0,
    ];

    $form['applicant_name'] = $application->getOwner()->profile_crm_indiv->entity->crm_name->view(['label' => 'inline']);
    $form['applicant_name']['#title'] = $this->t('Reference For');

    $form['applicant_dob'] = $ticket->date_of_birth->view([
      'label' => 'inline',
      'settings' => ['format_type' => 'html_date'],
    ]);

    $form['heading'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $this->t('This is a reference for an application to the @team team', [
        '@team' => $team ? $team->getName() : '',
      ]),
      '#weight' => -2,
    ];

    $form['intro'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('@name has requested a reference from you as part of their application to be on team at @event_name. Please answer the questions below to the best of your knowledge. If you are not %referee_name please disregard this reference.', [
        '@name' => $form['applicant_name'][0]['#markup'],
        '@event_name' => $event->label(),
        '%referee_name' => $this->entity->getRefereeName(),
      ]),
      '#weight' => -1,
    ];

    $form['actions']['submit']['#value'] = $this->t('Submit Reference');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Transition to the Submitted sate.
    $this->entity->markAsSubmitted();
    $result = parent::save($form, $form_state);
    $this->messenger()->addMessage($this->t('The reference has been submitted.'));
    return $result;
  }

  /**
   * Lays out the form into 2 columns.
   *
   * @param array $form
   *   Form array.
   *
   * @return array
   *   Modified form array.
   */
  public function layout(array $form) {
    // Add 2 columns, move everything into the left except the applicant's
    // name/dob/photo.
    $container = [
      '#type' => 'container',
      '#attributes' => ['class' => ['row']],
      'container_1' => [
        '#type' => 'container',
        '#attributes' => ['class' => ['col-md-8']],
      ],
      'container_2' => [
        '#type' => 'container',
        '#attributes' => ['class' => ['col-md-4']],
      ],
    ];

    // Move applicant details into the right-hand column.
    $applicant_details = [
      'applicant_title',
      'applicant_name',
      'applicant_dob',
    ];

    foreach ($applicant_details as $delta) {
      $container['container_2'][$delta] = $form[$delta];
      unset($form[$delta]);
    }

    $exclude = [
      'footer',
      'actions',
      'form_build_id',
      'form_token',
      'form_id',
    ];

    foreach (Element::children($form) as $delta) {
      if (!in_array($delta, $exclude)) {
        $container['container_1'][] = $form[$delta];
        unset($form[$delta]);
      }
    }

    $form['container'] = $container;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['layout'];
  }

}
