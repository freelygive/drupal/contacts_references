<?php

namespace Drupal\contacts_references\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\contacts_references\Entity\ReferenceType;
use Drupal\token\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for reference type forms.
 */
class ReferenceTypeForm extends EntityForm {

  /**
   * Tokens.
   *
   * @var \Drupal\token\Token
   */
  protected $token;

  /**
   * ReferenceTypeForm constructor.
   *
   * @param \Drupal\token\Token $token
   *   Tokens.
   */
  public function __construct(Token $token) {
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    if ($this->operation === 'add') {
      $form['#title'] = $this->t('Add Reference type');
    }

    $form['label'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('The human-readable name of this reference type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#maxlength' => 32,
      '#disabled' => !$this->entity->isNew(),
      '#machine_name' => [
        'exists' => [ReferenceType::class, 'load'],
      ],
      '#description' => $this->t('A unique machine-readable name for this reference type.'),
    ];

    $form['notification'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Notification Settings'),
    ];

    $form['notification']['subject'] = [
      '#title' => $this->t('Notification Subject'),
      '#type' => 'textfield',
      '#default_value' => $this->entity->get('subject'),
      '#maxlength' => 250,
      '#required' => TRUE,
      '#description' => $this->t('The subject of the notification email. You can use the tokens shown below.'),
    ];

    $form['notification']['body'] = [
      '#title' => $this->t('Notification Body'),
      '#type' => 'textarea',
      '#default_value' => $this->entity->get('body'),
      '#required' => TRUE,
      '#description' => $this->t('The content of the notification email. You can use the tokens shown below. Remember to include the URL for submitting a reference [reference:submission-url]. For team applications, you can use [reference:team_name] for the team name and [reference:event_name] for the event name.'),
      '#element_validate' => ['token_element_validate'],
      '#token_types' => [
        'site',
        'contacts_reference',
      ],
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $form['notification']['tokens'] = [
        '#type' => 'container',
        '#theme' => 'token_tree_link',
        '#token_types' => $form['notification']['body']['#token_types'],
        '#global_types' => FALSE,
      ];
    }
    else {
      $form['notification']['tokens'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#title' => $this->t('Available Tokens'),
        '#items' => $this->getAvailableTokens(),
        '#wrapper_attributes' => ['class' => 'container'],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save');
    $actions['delete']['#value'] = $this->t('Delete');
    $actions['delete']['#access'] = $this->entity->access('delete');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $t_args = ['%name' => $this->entity->label()];
    if ($status === SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('The reference type %name has been updated.', $t_args));
    }
    elseif ($status === SAVED_NEW) {
      $this->messenger()->addStatus($this->t('The reference type %name has been added.', $t_args));
      $this->logger('contacts_references')->notice('Added reference type %name.', $t_args);
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

  /**
   * Gets available tokens.
   *
   * @return array
   *   Array of tokens.
   */
  private function getAvailableTokens() {
    $available_tokens = [];
    foreach ($this->token->getInfo()['tokens']['contacts_reference'] as $token_id => $token) {
      $available_tokens[] = "[contacts_reference:{$token_id}] - {$token['name']}";
    }
    return $available_tokens;
  }

}
