<?php

namespace Drupal\contacts_references\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\contacts_references\ReferenceFieldHelper;
use Drupal\contacts_references\ReferenceSender;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the contacts_reference edit forms.
 */
class ReferenceReplaceForm extends ContentEntityForm {

  /**
   * Helper for finding reference fields on an entity.
   *
   * @var \Drupal\contacts_references\ReferenceFieldHelper
   */
  protected $referenceFieldHelper;

  /**
   * Reference email sender.
   *
   * @var \Drupal\contacts_references\ReferenceSender
   */
  protected $referenceSender;

  /**
   * The original reference, being replaced.
   *
   * @var \Drupal\contacts_references\Entity\ReferenceInterface
   */
  protected $originalReference;

  /**
   * The team application.
   *
   * @var \Drupal\contacts_events_teams\Entity\TeamApplication
   */
  protected $teamApplication;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, ReferenceFieldHelper $reference_field_helper, ReferenceSender $reference_sender) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->referenceFieldHelper = $reference_field_helper;
    $this->referenceSender = $reference_sender;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('contacts_references.reference_field_helper'),
      $container->get('contacts_references.sender')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    // Instead of using the reference passed in the URL, use it to create a new
    // one which will replace the existing one.
    $this->originalReference = parent::getEntityFromRouteMatch($route_match, $entity_type_id);
    $this->teamApplication = $this->originalReference->getTeamApplication();
    // Create a new reference of the same type.
    $new_reference = $this->entityTypeManager->getStorage('contacts_reference')->create([
      'bundle' => $this->originalReference->bundle(),
      'uid' => $this->originalReference->getOwnerId(),
      'application' => $this->teamApplication->id(),
    ]);

    return $new_reference;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\contacts_references\Entity\ReferenceType */
    $reference_type = $this->entityTypeManager->getStorage('contacts_reference_type')
      ->load($this->originalReference->bundle());

    $form['intro'] = [
      '#weight' => -1,
      '#markup' => $this->t('Replacing %reference_type for %user (Team: %team)', [
        '%reference_type' => $reference_type->label(),
        '%user' => $this->teamApplication->getOwner()->label(),
        // Directly chain from getTeam() because ReferenceAccessControlHandler
        // checks that the team exists.
        '%team' => $this->teamApplication->getTeam()->getName(),
      ]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);

    // After saving the new reference, replace reference on the team app.
    foreach ($this->referenceFieldHelper->findReferences($this->teamApplication) as $field_id => $reference) {
      if ($reference->id() == $this->originalReference->id()) {
        $this->teamApplication->set($field_id, $this->entity);

        /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state */
        // Send the application back to submitted state.
        $state = $this->teamApplication->get('state')->first();
        if ($state->getString() === 'requested') {
          $state->applyTransitionById('submit');
        }
        elseif (in_array($state->getString(), ['approved', 'rejected'])) {
          $state->applyTransitionById('reset');
        }

        $this->teamApplication->save();
        break;
      }
    }

    // Archive the original.
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state */
    $state = $this->originalReference->get('state')->first();
    $state->applyTransitionById('archive');
    $this->originalReference->save();

    $form_state->setRedirectUrl($this->teamApplication->toUrl('canonical', ['fragment' => 'references']));
  }

  /**
   * Sends the reference request.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function sendReferenceRequest(array $form, FormStateInterface $form_state) {
    $this->referenceSender->send($this->entity);
    $this->messenger()->addMessage($this->t('A reference request has been sent to %email', [
      '%email' => $this->entity->get('email')->value,
    ]));
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save and Send Reference Request'),
      '#submit' => ['::submitForm', '::save', '::sendReferenceRequest'],
      '#attributes' => ['class' => ['btn', 'btn-primary']],
    ];

    $actions['submit_no_reference_request'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save (No reference request)'),
      '#submit' => ['::submitForm', '::save'],
      '#attributes' => ['class' => ['btn', 'btn-primary']],
    ];

    $actions['cancel'] = [
      '#type' => 'link',
      '#url' => $this->teamApplication->toUrl('canonical', ['fragment' => 'references']),
      '#title' => $this->t('Cancel'),
      '#attributes' => ['class' => ['btn', 'btn-secondary']],
    ];
    return $actions;
  }

}
