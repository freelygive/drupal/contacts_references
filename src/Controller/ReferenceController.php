<?php

namespace Drupal\contacts_references\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Controller for routes around submitting references.
 */
class ReferenceController extends ControllerBase {

  /**
   * Checks access for the reference submission form.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function checkSubmitAccess(RouteMatchInterface $route_match, AccountInterface $account) {
    $reference_uuid = $route_match->getParameter('uuid');
    $reference_token = $route_match->getParameter('token');

    $valid_reference = FALSE;
    $hash_validated = FALSE;

    $results = $this->entityTypeManager()->getStorage('contacts_reference')
      ->loadByProperties(['uuid' => $reference_uuid]);

    if (count($results) == 1) {
      /** @var \Drupal\contacts_references\Entity\Reference $reference */
      $reference = reset($results);

      // Allowed if it's in the 'requested' state.
      // Also allow if it's been submitted (submitted/approved) so we can show
      // an 'already submitted' message.
      $allowed_statuses = ['requested', 'submitted', 'approved'];
      if (in_array($reference->get('state')->value, $allowed_statuses)) {
        $valid_reference = TRUE;
      }

      // Deny if the reference is for the currently logged in user, if not
      // anonymous.
      if (!$account->isAnonymous()) {
        if ($account->id() == $reference->getOwnerId()) {
          $valid_reference = FALSE;
        }
      }

      $hash_validated = hash_equals($reference->generateToken(), $reference_token);
    }

    return AccessResult::allowedIf($hash_validated && $valid_reference)
      ->setCacheMaxAge(0);
  }

  /**
   * Displays the submitted message.
   *
   * This is shown either after a reference has been submitted, or if the user
   * attempts to complete an already-submitted reference.
   */
  public function submitted() {
    $build['header'] = ['#markup' => '<h2>Reference Submitted</h2>'];
    $build['message'] = ['#markup' => '<p>Thank you for completing the reference - it has been successfully submitted.</p>'];
    return $build;
  }

}
