<?php

namespace Drupal\contacts_references;

use Drupal\contacts_references\Entity\ReferenceInterface;
use Drupal\contacts_references\Entity\ReferenceType;
use Drupal\Core\Language\Language;

/**
 * Sends references.
 *
 * @package Drupal\contacts_references
 */
class ReferenceSender {

  /**
   * Sends reference request emails.
   *
   * @param \Drupal\contacts_references\Entity\ReferenceInterface $reference
   *   The reference to send.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function send(ReferenceInterface $reference) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state */
    $state = $reference->get('state')->first();
    if (!$this->canSendReference($reference)) {
      throw new \Exception("Cannot send reference email when in the {$state->getString()} state.");
    }

    if ($state->getString() === 'pending') {
      $state->applyTransitionById('request');
      $reference->save();
    }

    // @todo inject.
    /** @var \Drupal\Core\Mail\MailManagerInterface $mail_man */
    $mail_man = \Drupal::service('plugin.manager.mail');
    /** @var \Drupal\token\Token $token */
    $token = \Drupal::service('token');
    $type = ReferenceType::load($reference->bundle());

    $data = ['contacts_reference' => $reference];
    $subject = $token->replace($type->get('subject'), $data);
    $body = $token->replace($type->get('body'), $data);

    $params = [
      'subject' => $subject,
      'body' => [$body],
    ];

    $mail_man->mail('contacts_references', 'reference_request', $reference->get('email')->value,
      Language::LANGCODE_NOT_SPECIFIED, $params);
    $reference->set('sent', \Drupal::time()->getRequestTime());
    $reference->save();
  }

  /**
   * Check the reference is in a state that can be sent.
   *
   * @param \Drupal\contacts_references\Entity\ReferenceInterface $reference
   *   The reference being checked.
   *
   * @return bool
   *   Whether the reference can be sent.
   */
  public function canSendReference(ReferenceInterface $reference) : bool {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state */
    $state = $reference->get('state')->first();

    // Reference email can be sent if:
    // - the reference is currently pending. We should transition to requested.
    // - the reference is currently requested. This is a re-send.
    if ($state->getString() !== 'pending' && $state->getString() !== 'requested') {
      return FALSE;
    }

    return TRUE;
  }

}
