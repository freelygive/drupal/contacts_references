<?php

namespace Drupal\contacts_references;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for reference entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class ReferenceHtmlRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($submit_route = $this->getSubmissionFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.submission_form", $submit_route);
    }

    if ($replace_route = $this->getReplaceFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.replace_form", $replace_route);
    }

    $collection->get("entity.{$entity_type_id}.edit_form")
      ->setDefault('_title', 'Reference Request');

    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getEditFormRoute($entity_type);
    // Replace the title to something useful.
    $route
      ->setDefault('_title_callback', NULL)
      ->setDefault('_title', 'Reference Request');

    return $route;
  }

  /**
   * Gets the submission-form route for secure reference submissions.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getSubmissionFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('submission-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('submission-form'));
      $route
        ->addDefaults([
          '_entity_form' => "{$entity_type_id}.submit",
          '_title' => 'Reference Request',
        ])
        ->setRequirement('_custom_access', '\Drupal\contacts_references\Controller\ReferenceController::checkSubmitAccess')
        ->setOption('no_cache', TRUE);

      return $route;
    }
  }

  /**
   * Gets the replace-form route for replacing a reference.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getReplaceFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('replace-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('replace-form'));
      $route
        ->addDefaults([
          '_entity_form' => "{$entity_type_id}.replace",
          '_title' => 'Replace Reference',
        ])
        ->setRequirement('_entity_access', 'contacts_reference.replace');

      return $route;
    }
  }

}
