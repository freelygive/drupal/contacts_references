INTRODUCTION
------------

  * This module extends the Contacts Events Teams module to allow you to
    collect references for users applying to join a team.

  * This module automatically sends emails to addresses collected from users
    with access to apply to join Teams on your site. As such, it can be used to
    send spam emails that may harm the reputation of your site. Careful
    consideration should be given to who you grant access to and your user
    verification process.

REQUIREMENTS
------------

  * This module requires the following modules:

    - Contacts Events Teams (/project/contacts_events)

INSTALLATION
------------

  * The Contacts Events Teams module should be enabled and configured before
    this module is enabled. At least one Team and Team Application Form should
    be created before following the rest of these instructions.

  * The module can be installed and enabled as usual. Visit
    https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

  * On install, the module creates a new Reference Type entity (a Default is
    created automatically), managed at /admin/structure/contacts-reference-type.
    New reference types can be added, and the entity is fieldable so extra
    fields can be added as required.

  * The Reference Type entity has a Default form display, which is presented to
    the user giving details of who should be contacted to provide a reference -
    by default this contains only the email address that the request should be
    sent to. It also has a Request form display, which is shown to the person
    providing the reference: by default this shows the email address added by
    the original user and a Name field which contains the name of the person the
    reference is for. Additional fields can be added to either display as
    required.

  * Before references can be collected, a new field must be added to the Team
    Application Form you want to collect references through: this can be added
    at /admin/structure/teams/forms/{form type}/fields. The field needs to be an
    entity reference that references the Reference entity. It should be made
    visible on the form display at
    /admin/structure/teams/forms/{form type}/form-display and set to use the
    Inline Entity Form display mode. This will allow the user providing the
    address to email to create a new Reference Type entity when filling in their
    team application. If this step is not completed, all Team Applications will
    be created at the "References Reviewed" state and will progress as usual for
    the Contacts Teams module.

  * On completing a Team Application Form, reference request emails will be sent
    to the email addresses on the form. These emails are configurable for each
    Reference Type, at
    /admin/structure/contacts-reference-type/manage/{reference type} but must
    contain the [contacts_reference:submission-url] token as this will be
    replaced with the URL the referee can complete their reference at.

  * Once a reference has been completed by the referee, the reference will be
    moved to the "References Received" state. References can be reviewed at
    /event/{event id}/teams/applications by clicking their View link. The
    reference can be approved or rejected in the References field group (this is
    separate to the approval or rejection of the Team application itself). Once
    approved or rejected, the Team application process continues as normal.

TROUBLESHOOTING
---------------

 * If you complete a Team Application Form and your application status jumps
   straight to "References Received", make sure you have followed the
   instructions for adding an Entity Reference field to the Team Application
   Form you are using.

 * If your users are being asked to provide referee details but are not asked
   to provide an email address for the reference to go to, make sure you have
   not removed the Email field from the Reference Type's Default form mode.

FAQs
----

 * I want to have an off-line reference process
   Whilst it would be possible to adapt the module using custom code, there is
   currently no manual process for updating the status of References and Team
   Applications. It would probably be easier to manage the reference process
   completely offline, and work it into the process of reviewing Team
   Applications without utilising this module.

MAINTAINERS
-----------

Current maintainers:
 * Andrew Belcher (andrewbelcher) - /u/andrewbelcher
 * Yan Loetzer (yanniboi) - /u/yanniboi
 * Paul Smith (MrDaleSmith) - /u/mrdalesmith

This project has been sponsored by:
 * Freely Give
   We’re FreelyGive, Drupal specialists developing Full Stack Customer
   Relationship Management (CRM) solutions your users will love.
   Visit https://freelygive.io/ for more information.
